FROM openjdk:8-jre-alpine
COPY target/*.jar /app.jar
CMD ["/usr/bin/java", "-jar", "-Dserver.port=8070", "/app.jar"]